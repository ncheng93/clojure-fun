(in-ns 'clojure-fun.core)

; http://www.cse.chalmers.se/edu/year/2013/course/TDA452_Functional_Programming/exercises/3/

(defn vec-remove
  "remove elem in coll"
  [coll pos]
  (vec (concat (subvec coll 0 pos) (subvec coll (inc pos)))))

(defn isPermutation [list1 list2]
  (cond 
    (and (zero? (count list1)) (zero? (count list2)))
      true
    (zero? (count list2))
      false
    :else
      (let [[head & tail] list1
            ind (.indexOf list2 head)]

        (if (= -1 ind)
          false
          (isPermutation tail (vec-remove list2 ind))))))

(defn containsDups [list1]
  (letfn [
    (helper [[h & t :as full] stash]
      (if (zero? (count full))
        false
        (if (= (stash h) nil) 
          (helper t (assoc stash h true))
          true)))]
    (helper list1 {})))

;lazy sequence of pascal rows
(def pascal
  (letfn [
    (nextRow [row]
      (conj ((reduce 
          (fn [[acc iter] _]
            ; [ acc :: row[iter] + row[iter-1] ]
            [(conj acc (+ (row iter) (row (dec iter)))) (inc iter)])

          ;start with first item in row, index set to second
          [[1] 1]

          ;since it is symmetrical, this removes last element (the 1)
          (reverse (drop 1 row))

        ; take 0th index to drop "iter" from accumulator
        ; attach 1 to end to complete row
        ) 0) 1))

    (pHelper [prev]
      (cons prev (lazy-seq (pHelper (nextRow prev)))))]
  (concat '([1] [1 1]) (pHelper [1 2 1]))))


(defn primesTo [n]
  (letfn [(crossOut [n theList]
    (filter #(not (and (= (mod % n) 0) (not= % n))) theList))]
  (reduce 
    (fn [acc elem]
      (crossOut elem acc))
    (range 1 n)
    (range 2 n))))