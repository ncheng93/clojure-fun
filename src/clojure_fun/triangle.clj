(in-ns 'clojure-fun.core)

(defn biggestPath [tri]
  (letfn [
    (getChildren [tri start]
      (let [x (start 0) y (start 1)]
        (if (< y (- (count tri) 1)) 
          ; not at bottom of triangle 
          [[x (+ y 1)] [(+ x 1) (+ y 1)]]

          ; at bottom of triangle
          []
         )))

    (biggestPathHelp [tri start]
      (let [x (start 0) 
        y (start 1)
        parent ((tri y) x)
        kids (getChildren tri start)]

        (if (= 0 (count kids))
          ; base case
          parent

          ; recursive case
          (let [left (biggestPathHelp tri (kids 0))
              right (biggestPathHelp tri (kids 1))]
            (+ parent (max left right))))))
      ]
    (biggestPathHelp tri [0,0])))