(in-ns 'clojure-fun.core)

(defn new-user [name age]
  {:name name :age age})

(defn addUser [usersRef user]
  (dosync
    (let [newId (inc (count @usersRef))] ;put this count inside dosync to ensure consistency
      (alter usersRef assoc newId user))))


(defn doRef []
  (do 
    (println (helper/askForHelp "Nick"))
    (def allUsers (ref {}))
    (addUser allUsers (new-user "bob" 5))
    (addUser allUsers (new-user "phil" 6))
    (println @allUsers)


    ; fun with agents
    (def agent1 (agent 0))
    (def agent2 (agent 0))

    (send agent1 + 50)
    (send agent2 + 60)

    ; probably wont have 50 and 60
    (println "immediate println:" @agent1 @agent2)

    ; create barrier that waits for both agents to finish
    (await agent1 agent2)

    ; will have 50 and 60
    (println "wait for agents to complete:" @agent1 @agent2)

    


    ; atoms

    (def counter (atom 0))
    (swap! counter + 1)
    (println @counter)

    ))