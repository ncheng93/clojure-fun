; this one is in the same namespace: clojure-fun.core
; I can use it in any other files of the same namespace
; by including the command (load "fact") where fact.clj
; is the file name of this file

(in-ns 'clojure-fun.core)

(defn factorial [n]
  (if (<= n 1)
    1
    (* n (factorial (- n 1)))))

; using & makes tail consume the rest of the list
(defn headAndTail [ [head & tail] ]
  (println head "and" tail))

; using :as collects the whole thing into one variable too
(defn headAndTail2 [ [head head2 & tail :as all] ]
  (println head ", " head2 " and " tail)
  (println all))
    
; infinite lazy sequence
(def naturalNums
  (letfn [(intsFrom [n] 
      (cons n (lazy-seq (intsFrom (inc n)))))]
    (intsFrom 0)))

(def fibonacci
  (letfn [(fibHelper [a b] 
      (cons b (lazy-seq (fibHelper b (+ a b)))))]
    (cons 0 (fibHelper 0 1))))