(in-ns 'clojure-fun.core)

(defn mergeLists [[h1 & t1 :as list1] [h2 & t2 :as list2]]
  (cond
    (nil? h1) list2
    (nil? h2) list1
    :default
      (if (> h1 h2)
          (cons h2 (mergeLists list1 t2))
          (cons h1 (mergeLists t1 list2)))))

(declare mergeSortSerial)
(declare mergeSortConcurrent)

(defn mergeSort 
  "merges concurrently if more than 1000
   elements, otherwise does it serially"
  [input]
  (if (> (count input) 1000)
      (mergeSortConcurrent input)
      (mergeSortSerial input)))

(defn mergeSortConcurrent [input]
  (if (= (count input) 1)
    ; base case
    input
    ; recursive case
    (let [middle (int (/ (count input) 2))
          front (take middle input)
          back  (drop middle input)
          sortedFront (future (mergeSort front))
          sortedBack  (future (mergeSort back))]
      (mergeLists @sortedFront @sortedBack))))

(defn mergeSortSerial [input]
  (if (= (count input) 1)
    ; base case
    input
    ; recursive case
    (let [middle (int (/ (count input) 2))
          front (take middle input)
          back  (drop middle input)
          sortedFront (mergeSortSerial front)
          sortedBack  (mergeSortSerial back)]
      (mergeLists sortedFront sortedBack))))
