(in-ns 'clojure-fun.core)

;rounds to nearest even number
(defn even [n]
  (* 2 (int (/ n 2))))

(def FILL_SYMBOL "X")
(def EMPTY_SYMBOL " ")

(defn printCanvas[canvas]
  (println 
    (reduce 
      (fn [acc row] 
        (str acc 
          (reduce 
            (fn [acc cell] (str acc (if (= cell 1) FILL_SYMBOL EMPTY_SYMBOL)))
            "" 
           row) "\n"))
      ""
      canvas)))

; replaces row[start : start+len] with 1's
(defn replaceRow [start len row]
  (if (< start (count row)) 
    (if (= len 0) 
      row
      (replaceRow (inc start) (dec len) (assoc row start 1)))
    
    row))


; creates a triangle of 1's at (x,y) with bottom
; sidelength w
(defn createTriangle [x y w canvas]
  (if (>= y 0)
    (if (= 0 w)
      canvas
      (let [newRow (replaceRow x w (canvas y))
            newCanvas (assoc canvas y newRow)]

            (createTriangle (inc x) (dec y) (- w 2) newCanvas)))

    canvas))

; draws a sierpinski triangle at (x,y) of bottom
; sidelength w, with levels of recusion
(defn sierp [x y w levels canvas]
  (if (= 0 levels)
    ; base case
    (createTriangle x y w canvas)
    ; recursive case
    (let [halfW (even (/ w 2))
          quarW (even (/ w 4))

          oneTri (sierp x           y           halfW (dec levels) canvas)
          twoTri (sierp (+ x halfW) y           halfW (dec levels) oneTri)
          thrTri (sierp (+ x quarW) (- y quarW) halfW (dec levels) twoTri)]

      thrTri)))