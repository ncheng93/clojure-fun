(ns clojure-fun.core
  (:gen-class)
  (:import (javax.swing JOptionPane))
  (:require [clojure-fun.helper :as helper]))

(load "fact" "quicksort" "bfs" "triangle" "mut" "sierp" "chalmers" "mergesort")

;(use 'clojure.string)
;(use 'clojure-fun.helper)
;(require '(clojure [string :as string-lib]))
; (require '(clojure-fun [helper :as helper]))

;whats the difference betwen import, load, use, require?
; import loads in java classes for use
; require gets access to clojure libraries and namespaces that I make
; use takes a clojure library/namespace and dumps its functions here
; load takes the contents of a file and defines its functions here which
;    is useful for splitting a namespace over multiple files


(defn strToNum [str]
  (. Integer parseInt str))

(defn -main 
  "this is a docstring, wow!"
  [& args]
  ;((JOptionPane/showMessageDialog nil "Hello World"))

  ;(println (askForHelp "Nick"))
  (println (helper/askForHelp "Nick"))

  ; Simple exercises!
  (println "Woo: " (factorial 5))
  (println "list: " (quickSort [5 3 8 3 6 23 58 9]))


  ; Breadth First Search
  (def theMap
  [[0 0 0 0 0 0]
   [0 0 0 0 0 0]
   [1 1 1 1 0 0]
   [0 0 0 1 0 0]
   [1 1 1 1 0 0]
   [0 0 0 0 0 0]])

  (println (_bfs theMap [0,0] [5,5]))

  (println (_bfs theMap [0,0] [2,3]))

  (def table {:key "value"})

  ; Project Euler Triangle Problem
  (def triangle (with-open [rdr (clojure.java.io/reader "resources/tri.txt")]
    (let [lines (line-seq rdr)]
      (doall 
        (into [] (map 
          (fn [line] (into [] (map strToNum (clojure.string/split line #"\s"))))
          lines))))))

  (println (biggestPath triangle))

  ; Deconstructing
  (headAndTail [1 2 3 4 5])
  (headAndTail2 [1 2 3 4 5])

  ; Concurrency!!!
  (doRef)
  
  ; Lazy Sequences
  (println (take 10 naturalNums))
  (println (take 30 fibonacci))

  (println "")
  (println "")
  (println "")
  (println "")


  ; Sierpinski's Triangle
  (def canvas 
    (let [width 60
          height 30
          row (into [] (replicate width 0))]
      (into [] (replicate height row ))))


  (printCanvas (sierp 5 28 50 1 canvas))
  (printCanvas (sierp 5 28 50 2 canvas))

  ; Chalmers exercises
  (println (isPermutation [1 4 3] [4 3 1]))
  (println (isPermutation [1 4 4] [4 4 1]))
  (println (not (isPermutation [1 4 4] [4 3 1])))
  (println (not (isPermutation [1 4] [4 3 1])))
  (println (not (isPermutation [1 4 3] [4 1])))
  (println (not (isPermutation [1 4 3] [])))
  (println (not (isPermutation [] [1 4 3])))

  (println (not (containsDups [1 3 4 5 7 8 20 9])))
  (println (not (containsDups [1])))
  (println (not (containsDups [])))
  (println (containsDups [1 3 4 5 7 8 20 1]))

  (println (take 10 pascal))

  (println (primesTo 100))

  ; concurrent merge sort
  (def back (into [] (reverse (range 3000))))

  ;(println (mergeSort [1 5 3 77 33 53 645 67 35 645 75]))
  (println "serial sort of 3000 elements")
  (time (count (mergeSortSerial back)))
  (println "concurrent sort of 3000 elements")
  (time (count (mergeSort back)))
 


  (shutdown-agents)
)
