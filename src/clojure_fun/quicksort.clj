(in-ns 'clojure-fun.core)

(defn concatvec [A B]
  (into [] (concat A B)))

(defn _partition [A pivot]
  (reduce
    (fn [acc elem]
      (if (< elem pivot)
        [(conj (first acc) elem) (second acc)]
        [(first acc) (conj (second acc) elem)]  ))
    [[][]]
    A))

(defn quickSort [A]
  (if (= 0 (count A))
    []
    (let [pivot (last A)
      [head tail] (_partition (butlast A) pivot)
      sortedHead (quickSort head)
      sortedTail (quickSort tail)]
      (concatvec (conj sortedHead pivot) sortedTail))))