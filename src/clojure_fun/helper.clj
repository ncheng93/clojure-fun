(ns clojure-fun.helper)

(defn askForHelp [name]
  (str name " needs help!"))