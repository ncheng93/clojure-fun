(in-ns 'clojure-fun.core)

(defn _bfs [G start end]
  (letfn [
    (getNeighbors [point]
      (let [[x,y] point
            dirs [[-1 0] [0 -1] [0 1] [1 0]]
            height (count G)
            width (count (first G))]
        (reduce
          (fn [acc elem]
            (let [tx (+ x (first elem))
                  ty (+ y (second elem))]
              (if (and (>= tx 0) (>= ty 0) (< tx width) (< ty height) (= 0 (nth (nth G ty) tx)))
                ;tx,ty in bounds and not a wall
                (conj acc [tx ty])
                ;else
                acc)))
          []
          dirs)))

    (bfsHelper [Q V]
      (if (= 0 (count Q))
        ;Queue empty
        false

        ;Queue not empty
        (let [Qtail (rest Q)
              Qhead (first Q)
              neighbors (getNeighbors Qhead)]

          (if (= end Qhead)
            ;found goal
            true

            ;keep searching
            (let [[newV newQ] (reduce
                  (fn [[accV accQ] elem]
                    (if (= -1 (.indexOf accV elem))
                      ;not in visited list
                      [(conj accV elem) (conj accQ elem)]
                      ;in visited list
                      [accV accQ]))
                  [V Qtail]
                  neighbors)]
              (bfsHelper newQ newV))))))]
    (bfsHelper [start] [start])))





(defn getNeighbors [G point]
  (let [[x,y] point
        dirs [[-1 0] [0 -1] [0 1] [1 0]]
        height (count G)
        width (count (first G))]

    (reduce
      (fn [acc elem]
        (let [tx (+ x (first elem))
              ty (+ y (second elem))]

          (if (and (>= tx 0) (>= ty 0) (< tx width) (< ty height) (= 0 (nth (nth G ty) tx)))
            (conj acc [tx ty])
            acc)))
      []
      dirs)))
